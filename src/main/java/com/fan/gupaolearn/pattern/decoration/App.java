package com.fan.gupaolearn.pattern.decoration;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        NavigationBar navigationBar;

        // 未登录时
        navigationBar = new BaseNavigationBar();
        System.out.println("未登录时的："+navigationBar.getNavigationBar());

        // 登录了
        navigationBar = new LoginNavigationBar(navigationBar);
        System.out.println("登录时的："+navigationBar.getNavigationBar());


    }
}
