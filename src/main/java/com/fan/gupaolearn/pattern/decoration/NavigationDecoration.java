package com.fan.gupaolearn.pattern.decoration;

public abstract class NavigationDecoration extends NavigationBar {

    private NavigationBar navigationBar;

    public NavigationDecoration(NavigationBar navigationBar) {
        this.navigationBar = navigationBar;
    }

    @Override
    protected String getNavigationBar(){
        return navigationBar.getNavigationBar();
    }
}

