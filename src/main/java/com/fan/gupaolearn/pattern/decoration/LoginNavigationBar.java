package com.fan.gupaolearn.pattern.decoration;

public class LoginNavigationBar extends NavigationDecoration {

    public LoginNavigationBar(NavigationBar navigationBar) {
        super(navigationBar);
    }

    @Override
    protected String getNavigationBar() {
        return super.getNavigationBar() + "作业 题库 成长墙 ";
    }
}
